package pe.com.bean;

public class IntegrationResponseAgencia {
	
	private HeaderResponse responseHeader;
	private BodyResponseAgencia responseBody;

	public HeaderResponse getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(HeaderResponse responseHeader) {
		this.responseHeader = responseHeader;
	}

	public BodyResponseAgencia getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(BodyResponseAgencia responseBody) {
		this.responseBody = responseBody;
	}

}
