package pe.com.bean;

public class Agencia {
	
	private String agencia;
	private String distrito;
	private String provincia;
	private String departamento;
	private String direccion;
	private double lat;
	private double lon;
	
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public String getDirecci�n() {
		return direccion;
	}
	public void setDirecci�n(String direcci�n) {
		this.direccion = direcci�n;
	}
	
	
	

}
