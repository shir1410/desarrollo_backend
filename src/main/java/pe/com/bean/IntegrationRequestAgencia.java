package pe.com.bean;

public class IntegrationRequestAgencia {

	private Header requestHeader;
	private BodyAgencia requestBody;

	public Header getRequestHeader() {
		return requestHeader;
	}

	public void setRequestHeader(Header requestHeader) {
		this.requestHeader = requestHeader;
	}

	public BodyAgencia getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(BodyAgencia requestBody) {
		this.requestBody = requestBody;
	}

}
