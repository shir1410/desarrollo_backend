package pe.com.bean;

public class ResponseAgencia {
	
	private IntegrationResponseAgencia integrationResponse;

	public IntegrationResponseAgencia getIntegrationResponse() {
		return integrationResponse;
	}

	public void setIntegrationResponse(IntegrationResponseAgencia integrationResponse) {
		this.integrationResponse = integrationResponse;
	}

}
