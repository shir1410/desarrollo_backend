package pe.com.bean;

public class Solicitud {

	private String nroSolicitud;
	private String fechaInicio;
	private String usuario;
	private double nroHoras;
	private int identificadorTiempo;

	public String getNroSolicitud() {
		return nroSolicitud;
	}

	public void setNroSolicitud(String nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public double getNroHoras() {
		return nroHoras;
	}

	public void setNroHoras(double nroHoras) {
		this.nroHoras = nroHoras;
	}

	public int getIdentificadorTiempo() {
		return identificadorTiempo;
	}

	public void setIdentificadorTiempo(int identificadorTiempo) {
		this.identificadorTiempo = identificadorTiempo;
	}

}
