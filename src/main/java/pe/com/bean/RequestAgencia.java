package pe.com.bean;

public class RequestAgencia {
	
	private IntegrationRequestAgencia integrationRequest;

	public IntegrationRequestAgencia getIntegrationRequest() {
		return integrationRequest;
	}

	public void setIntegrationRequest(IntegrationRequestAgencia integrationRequest) {
		this.integrationRequest = integrationRequest;
	}

}
