package pe.com.wservices;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import pe.com.bean.Agencia;
import pe.com.bean.BodyResponseAgencia;
import pe.com.bean.HeaderResponse;
import pe.com.bean.IntegrationResponseAgencia;
import pe.com.bean.RequestAgencia;
import pe.com.bean.ResponseAgencia;

@Path("v1")
public class DetalleAgencia {
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("agencias")
	
	public ResponseAgencia detalleAgencia(RequestAgencia requestAgencia) {
		
		ResponseAgencia responseGeneral = new ResponseAgencia();
		IntegrationResponseAgencia integrationResponse = new IntegrationResponseAgencia();
		BodyResponseAgencia bodyResponseAgencia =  new BodyResponseAgencia() ;
		HeaderResponse headerResponse = new HeaderResponse();
			headerResponse.setIdSesion(requestAgencia.getIntegrationRequest().getRequestHeader().getIdSesion());
			headerResponse.setIdOperacion(requestAgencia.getIntegrationRequest().getRequestHeader().getIdOperacion());
			headerResponse.setCodigoServicio("01");
			headerResponse.setEstadoServicio("OK");
			String agenciaTemp = requestAgencia.getIntegrationRequest().getRequestBody().getAgencia();
		
			if(agenciaTemp!=null) {
				headerResponse.setMensajeServicio("Se obtuvieron datos exitosamente");
				JsonParser parser = new JsonParser();
				java.io.InputStream inputStream = null;
				inputStream = getClass().getClassLoader().getResourceAsStream("agencias.json");
			
				Reader reader = new InputStreamReader(inputStream);
				JsonElement rootElement = parser.parse(reader);
				Gson gson = new Gson();  
				Agencia[] founderArray = gson.fromJson(rootElement, Agencia[].class);;       
	        
				for(Agencia a : founderArray) {
					if(a.getAgencia().equals(agenciaTemp)){
						bodyResponseAgencia.setAgencia(a);
						break;
					}
				}
				//bodyResponseAgencia.setAgencia(founderArray[1]);
	        
			} else {
			headerResponse.setMensajeServicio("agencia vac�a");
			}
			
		integrationResponse.setResponseHeader(headerResponse);
		integrationResponse.setResponseBody(bodyResponseAgencia);
		responseGeneral.setIntegrationResponse(integrationResponse);

		return responseGeneral;
	}

}
